import { Component, OnInit, OnDestroy } from '@angular/core';
import {AppareilService} from '../services/appareil.service';
import { Subscription, of } from 'rxjs';

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.css']
})
export class AppareilViewComponent implements OnInit, OnDestroy {
  
  appareils: any[];
  appareilSubscription: Subscription;
  
  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(
      () => {
        resolve(date);
      }, 2000
    );
  });

  constructor(private appareilService: AppareilService) {}
  
  onSave() {
    this.appareilService.saveAppareilsToServer();
  }

  ngOnInit(){
    
    this.appareilSubscription = 
    this.appareilService.appareilSubject.subscribe(
      (appareils: any []) => {
        this.appareilService.getAppareilsFromServer();
        this.appareils = appareils;
      }
    );
    this.appareilService.emitAppareilSubject();
  }

  onAllumer() {
    this.appareilService.switchOnAll();
    this.onSave();
  }

  onEteindre() {
    if(confirm('Etes-vous sûr de vouloir éteindre tous vos appareils ?')) {
      this.appareilService.switchOffAll();
      this.onSave();
    } else {
      return null;
    }
  }

  ngOnDestroy(){
    this.appareilSubscription.unsubscribe();
  }

}
